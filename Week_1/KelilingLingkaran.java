public class KelilingLingkaran {

 public static void main(String[] args) {
     // No. 2 Keliling Lingkaran
     double keliling, phi = 3.14;
     int r;

     //DIAMETER 10 JADI R = 5
     r = 5;
     keliling = 2 * phi * r;
     System.out.println("Keliling Lingkaran : " +String.format("%.2f", keliling));
 }
}
